
/**
* Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
* 
* Используя SWAPI, вывести информацию по всех планетам с пагинацией и возможностью просмотреть доп.
* информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
* 
* Данные для отображения в карточке планеты:
* 1. Наименование (name)
* 2. Диаметр (diameter)
* 3. Население (population)
* 4. Уровень гравитации (gravity)
* 5. Природные зоны (terrain)
* 6. Климатические зоны (climate)
* 
* При клике по карточке отображаем в модальном окне всю информацию 
* из карточки, а также дополнительную:
* 1. Список фильмов (films)
* - Номер эпизода (episode_id)
* - Название (title)
* - Дата выхода (release_date)
* 2. Список персонажей 
* - Имя (name)
* - Пол (gender)
* - День рождения (birth_year)
* - Наименование родного мира (homeworld -> name)
* 
* Доп. требования к интерфейсу:
* 1. Выводим 10 карточек на 1 странице
* 2. Пагинация позволяет переключаться между страницами, выводить общее количество страниц и текущие выбранные
* элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
* 3. Используем Bootstrap 4 для создания интерфейсов.
* 4. Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет
* информацию о них в един
*/


const prevPage = document.querySelector(".js-prev-page");
const nextPage = document.querySelector(".js-next-page");
const curentPage = document.querySelector(".js-curent-page");
const elementCurenttValue = curentPage.getAttribute("page-value");
const elementsCount = document.querySelector(".js-ememnts-count");
const parent = document.querySelector(".js-container");
const ResetList = document.querySelector(".js-movies");
const elementsPerPage = 10;
const elementsTotal = document.querySelector(".js-ememnts-count");
const resetCaracters = document.querySelector(".js-caracters");
    
// обработчик нажатия на кнопку "подробнее"
parent.addEventListener("click",  
  e => {
    if(e.target.classList.contains("js-modal-info")) {
    const elementTarget = e.target.getAttribute("data-planet");
    ResetList.innerHTML = "";
    resetCaracters.innerHTML = "";
    addInfo(elementTarget);
    addInfoCaracters(elementTarget);
    };
  }
);

// передает параметры для отслеживания номера след стр
const showElementsNumbers = function (number) {
  const numberTotal = elementsTotal.getAttribute("total"); 
  const elementsCountTo = elementsCount.getAttribute("to");
  const elementsCountFrom = elementsCount.getAttribute("from");
  const newTo = Number(elementsCountTo) + elementsPerPage;
  const newFrom = Number(elementsCountFrom) + elementsPerPage;
  elementsCount.setAttribute("to", newTo);
  elementsCount.setAttribute("from", newFrom);
  const countNumbers = document.querySelector(".js-ememnts-count");
  countNumbers.innerHTML = `${newFrom}-${newTo}/${numberTotal}`;
}
// передает параметры для отслеживания номера пред стр
const showElementsNumbersPrev = function (number) {
  const numberTotal = elementsTotal.getAttribute("total"); 
  const elementsCountTo = elementsCount.getAttribute("to");
  const elementsCountFrom = elementsCount.getAttribute("from");
  const newTo = Number(elementsCountTo) - elementsPerPage;
  const newFrom = Number(elementsCountFrom) - elementsPerPage;
  elementsCount.setAttribute("to", newTo);
  elementsCount.setAttribute("from", newFrom);
  const countNumbers = document.querySelector(".js-ememnts-count");
  countNumbers.innerHTML = `${newFrom}-${newTo}/${numberTotal}`;
}

// получает значение текущей страницы
const getValue = curPage => {
  const elementCurenttValue = curentPage.getAttribute("page-value") ;
  const toNumber = Number(elementCurenttValue);
  return toNumber;
};

// обработчик нажатия на кнопку "previous"
nextPage.addEventListener("click",  
  e => { 
    if(e.target.classList.contains("js-next-page")) {
      checkValue(getValue());
    };
  }
);

// обработчик нажатия на кнопку "next"
prevPage.addEventListener("click",  
  e => {
    if(e.target.classList.contains("js-prev-page")) {
      checkValuePrev(getValue());
    };
  }
);

// очищает стр. показ текущее число стр. педеает данные для работы с атрибутами текущ стр
const preparePrevPage = number => {
  parent.innerHTML = "";
  const newNumber = number - 1;
  curentPage.innerHTML= newNumber;
  showElementsNumbersPrev(newNumber);
  nextPage.parentNode.classList.remove("disabled");
};

// проверяет текущее значение чтобы блокироавть кнопку пред. стр или нет
const checkValuePrev = function (number) {
  if (number > 2) {
    preparePrevPage(number);
    prevPage.parentNode.classList.remove("disabled");
    return getPrevPage(number) ;
  };
  preparePrevPage(number);
  prevPage.parentNode.classList.add("disabled");
  getPrevPage(number);
}

//проверяет номер стр. блокировать кнопу сл, пред или нет
const checkValue = function (number) {
  const numberTotal = elementsTotal.getAttribute("total"); 
  const elementsAmount = Number(numberTotal);
  const pagesMax = elementsAmount/10;
  if (number < pagesMax-1) {
    parent.innerHTML = "";
    const newNumber = number + 1;
    curentPage.innerHTML= newNumber;
    showElementsNumbers(newNumber);
    prevPage.parentNode.classList.remove("disabled");
    return getNextPage(number) ;
  };
  parent.innerHTML = "";
  const newNumber = number + 1;
  curentPage.innerHTML= newNumber;
  showElementsNumbers(newNumber);
  getNextPage(number);
  nextPage.parentNode.classList.add("disabled");
}

// передает на отрисовку все стр 
const addAll = function () {
  const total = document.querySelector(".js-ememnts-count");
  const atributeTotal = total.getAttribute("total");
  const pagesTotal = atributeTotal / elementsPerPage;
  const url2 = "http://swapi.dev/api/planets/?page=";
  for(let i = 1; i <= pagesTotal; i++) {
    const newUrl = url2 + i;
    getPlanets(newUrl) ;
  };
};

// обработчик на нажатие кнопки "показать все планеты"
const showAllButton = document.querySelector(".js-show-all");
showAllButton.addEventListener("click",  
  e => {
  if(e.target.classList.contains("js-show-all-avlb")) {
    parent.innerHTML = "";
    const buttonText = document.querySelector(".js-show-all");
    buttonText.innerHTML = "На первую стр.";
    buttonText.classList.remove("js-show-all-avlb");
    prevPage.parentNode.classList.add("disabled");
    nextPage.parentNode.classList.add("disabled");
    curentPage.parentNode.classList.add("disabled");
    const numberTotal = elementsTotal.getAttribute("total"); 
    elementsCount.innerHTML = `${numberTotal}/${numberTotal}`
    return addAll()
    };
    parent.innerHTML = ""
    const buttonText = document.querySelector(".js-show-all");
    buttonText.classList.add("js-show-all-avlb");
    buttonText.innerHTML = "Показать все планеты";
    nextPage.parentNode.classList.remove("disabled");
    curentPage.parentNode.classList.remove("disabled");
    const numberTotal = elementsTotal.getAttribute("total"); 
    curentPage.innerHTML= 1;
    elementsCount.innerHTML = `1-${elementsPerPage}/${numberTotal}`
     getPlanets(url);
  }
);

// передает адрес пред. стр. для отрисовки, записывает номер стр
const getNextPage = function (num) {
  const covertedNumber = Number(num);
  const nextPageValue = covertedNumber + 1;
  const nextPageButton = document.querySelector(".js-curent-page");
  nextPageButton.setAttribute("page-value", nextPageValue);
  const url = "http://swapi.dev/api/planets/?page=";
  const nextPageUrl = url + nextPageValue;
  getPlanets(nextPageUrl);
};

// передает адрес след. стр. для отрисовки, записывает номер стр
const getPrevPage = function (num) {
  const covertedNumber = Number(num);
  const nextPageValue = covertedNumber - 1;
  const nextPageButton = document.querySelector(".js-curent-page");
  nextPageButton .setAttribute("page-value", nextPageValue);
  const url = "http://swapi.dev/api/planets/?page="
  const nextPageUrl = url + nextPageValue;
  getPlanets(nextPageUrl);
};

// создает html  с карточкой
const fillCard = function (planet) {
  const newElem = document.createElement("div");
  newElem.classList.add("js-card");
  newElem.innerHTML = 
    `
    <div class="card" style="width: 20rem;">
      <div class="card-body custom-card">
        <h5 class="card-title"><span>${planet.name}</span></h5>
        <ul class="list-group custom-list">
          <li class="list-group-item d-flex justify-content-between align-items-center diametr">
            <p>Диаметр</p>
            <span class="badge bg-primary rounded-pill value="${planet.diameter} custom-badge">${planet.diameter}</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <p>Население</p>
            <span class="badge bg-primary rounded-pill custom-badge">${planet.population}</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <p>Уровень гравитации</p>
            <span class="badge bg-primary rounded-pill custom-badge">${planet.gravity}</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <p>Природные зоны</p>
            <span class="badge bg-primary rounded-pill custom-badge">${planet.terrain}</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <p>Климатические зоны</p>
            <span class="badge bg-primary rounded-pill custom-badge">${planet.climate}</span>
          </li>
        </ul>
        <button type="button" class="btn btn-primary js-modal-info custom-button-more" data-bs-toggle="modal" data-bs-target="#exampleModal" data-planet="${planet.url}">
          Подробнее
        </button>
      </div>
    </div>
    `
   return parent.appendChild(newElem); 
};

//приходит ответ с сервера, передает макс кол-во стр, отрисовывает каждую карточку с планетой
const getPlanets = async (url) => { 
  const response = await fetch(url);
  const data = await response.json();
  const planets = data.results;
  for(let i = 0; i < planets.length; i++) {
    fillCard(planets[i]);
  }
};

//добавляет инфоримацию о планете
const addInfo = async (url) => {
  const infoPlanet = url
  const response = await fetch(infoPlanet);
  const dataPlanet = await response.json();
  const planet = dataPlanet;
  const planetName = document.querySelector(".js-modal-title");
  planetName.innerHTML = planet.name;
  const diameter = document.querySelector(".js-diameter");
  diameter.innerHTML = planet.diameter;
  const population = document.querySelector(".js-population");
  population.innerHTML = planet.population;
  const gravity = document.querySelector(".js-gravity");
  gravity.innerHTML = planet.gravity;
  const terrain = document.querySelector(".js-terrain");
  terrain.innerHTML = planet.terrain;
  const climate = document.querySelector(".js-climate");
  climate.innerHTML = planet.climate;
  const films = planet.films;
  const newArray = films.map(function (value) {
    return fetch(value).then(res => res.json());
  })
  
  Promise.all(newArray)
  .then(planets => {
    for(let i = 0; i < planets.length; i++) {
    const parentFilms = document.querySelector(".js-movies");
    const newElemFilms = document.createElement("ul");
    newElemFilms.innerHTML = `
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Номер эпизода</p>
        <span class="badge bg-primary rounded-pill js-episode-number">${planets[i].episode_id}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Название</p>
        <span class="badge bg-primary rounded-pill js-film-name">${planets[i].title}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Дата выхода</p>
        <span class="badge bg-primary rounded-pill js-relise">${planets[i].release_date}</span>
      </li>
    `
    parentFilms.appendChild(newElemFilms); 
        }  
      })
};

// считает кол-во общее кол-во элементов
const url = 'https://swapi.dev/api/planets';
  const getPlanetTotal = async (url) => { 
  const response = await fetch(url);
  const data = await response.json();
  const planetsAmount = data.count;
  const elementsTotal = document.querySelector(".js-ememnts-count");
  elementsTotal.setAttribute("total", planetsAmount); 
};

// получает информацию о персонаже
const addInfoCaracters = async (url) => {
  const infoPlanet = url;
  const response = await fetch(infoPlanet);
  const dataPlanet = await response.json();
  const planet = dataPlanet;
  const residents = planet.residents;
  const newArray = residents.map(function (value) {
     return fetch(value).then(res => res.json());
  });
  
  Promise.all(newArray)
  .then((residents) => {
     getCaracterInfo(residents);
  });
};

//добавляет инфо html о персонаже, и инфу о homeworld
const getCaracterInfo = async (resedentsArray) => {
  const array = []
  const residents = resedentsArray;
  for(let i = 0; i < residents.length; i++) {
    const request = residents[i].homeworld;
    fetch(request)
    .then(res => res.json())
    .then(res => residents[i].homeworld = res.name)
    .then(res = res=residents[i])
    .then(() =>  {       
      array.push(residents[i])
    })
    .then(() =>  {
          const parentFilms = document.querySelector(".js-caracters");
      const newElemFilms = document.createElement("ul");
      newElemFilms.innerHTML = `
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Имя</p>
        <span class="badge bg-primary rounded-pill">${residents[i].name}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Пол</p>
        <span class="badge bg-primary rounded-pill">${residents[i].gender}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>День рождения</p>
        <span class="badge bg-primary rounded-pill">${residents[i].birth_year}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <p>Наименование родного мира</p>
        <span class="badge bg-primary rounded-pill">${residents[i].homeworld}</span>
      </li>
      `;
      parentFilms.appendChild(newElemFilms); 
    });    
  };
};

(async () => {
  await getPlanetTotal(url);
  await getPlanets(url);
})();

